<div align="center">
    <a href="https://github.com/zhaofeng092/python_auto_office"> <img src="https://badgen.net/badge/Github/%E7%A8%8B%E5%BA%8F%E5%91%98?icon=github&color=red"></a>
    <a href="http://t.cn/A6Gkrbzw"> <img src="https://badgen.net/badge/follow/%E5%85%AC%E4%BC%97%E5%8F%B7?icon=rss&color=green"></a>
    <a href="https://space.bilibili.com/259649365"> <img src="https://badgen.net/badge/pick/B%E7%AB%99?icon=dependabot&color=blue"></a>
    <a href="https://mp.weixin.qq.com/s/CadAaJUTUlXmTxJAjFUfPQ"> <img src="https://badgen.net/badge/join/%E4%BA%A4%E6%B5%81%E7%BE%A4?icon=atom&color=yellow"></a>
</div>


> (建议加入群聊：[交流群](https://mp.weixin.qq.com/s/CadAaJUTUlXmTxJAjFUfPQ) ，每次更新资源，兆锋都会在群里通知哟~)

### 电子书网站：

- [传送门](https://mp.weixin.qq.com/s/qv5qOaG9BOCZHleg9gXtQA)
- 见链接中第一个链接

### PDF编辑器：

- 在线软件：[传送门](https://mp.weixin.qq.com/s/qv5qOaG9BOCZHleg9gXtQA) ，PDF万能转换，见文中第2个网站
- 离线软件：[传送门](http://www.xitongtiandi.net/soft_yy/9942.html) ，限时删除，抓紧安装~



### 👉更多：[办公神器 · 合集](https://gitee.com/zhaofeng092/python_auto_office/blob/master/%E5%85%B3%E9%94%AE%E8%AF%8D/%E7%BE%A4%E8%81%8A/%E6%9C%80%E6%96%B0%E6%95%99%E7%A8%8B/%E5%8A%9E%E5%85%AC%E7%A5%9E%E5%99%A8.md)

> (建议加入群聊：[交流群](https://mp.weixin.qq.com/s/CadAaJUTUlXmTxJAjFUfPQ) ，每次更新资源，兆锋都会在群里通知哟~)



## → 🚀




##### 📱社区资源仓库

<img src="https://img-blog.csdnimg.cn/20201231105911656.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMTUxNw==,size_16,color_FFFFFF,t_70#pic_center" alt="csdn资源仓库" style="zoom:50%;" />

##### 🆗社区交流群

<img src="https://img-blog.csdnimg.cn/20210102004119705.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMTUxNw==,size_16,color_FFFFFF,t_70#pic_center" alt="交流群" style="zoom:50%;" />



##### 📚知识星球

<img src="https://img-blog.csdnimg.cn/20210109190431333.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMTUxNw==,size_16,color_FFFFFF,t_70#pic_center" style="zoom: 80%;" />