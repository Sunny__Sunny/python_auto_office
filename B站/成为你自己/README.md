<div align="center">
    <a href="http://t.cn/A6Gkrbzw"> <img src="https://badgen.net/badge/follow/%E5%85%AC%E4%BC%97%E5%8F%B7?icon=rss&color=green"></a>
    <a href="https://space.bilibili.com/259649365"> <img src="https://badgen.net/badge/pick/B%E7%AB%99?icon=dependabot&color=blue"></a>
    <a href="https://mp.weixin.qq.com/s/CadAaJUTUlXmTxJAjFUfPQ"> <img src="https://badgen.net/badge/join/%E4%BA%A4%E6%B5%81%E7%BE%A4?icon=atom&color=yellow"></a>
</div>

## 心理视频的链接

1. 🔥[武志红《成为你自己》](https://www.bilibili.com/video/BV1mi4y1j7DF)



## 心理测试的链接

1. 🌟[潜在心理创伤测试](http://www.urlort.cn/2SIUm2)
2. [什么能力很重要，但大多数人都没有？](http://www.urlort.cn/2ToHj3)
3. [你延续着哪些来自童年的性格陷阱？](http://www.urlort.cn/2SIUK8)
4. [发现属于自己的职场优势](http://www.urlort.cn/2S7zo0)
5. [恋爱心理成熟度评估](http://www.urlort.cn/2SIV69)
6. [童年阴影评估](http://www.urlort.cn/2Qbuge)



## 心理分析的交流群

👉关注公众号：Python自动化办公社区，在后台发送：**心理分析**，即可加入群聊~



## → 🚀社区资源仓库&社区交流群：


##### 📱社区资源仓库

<img src="https://img-blog.csdnimg.cn/20201231105911656.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMTUxNw==,size_16,color_FFFFFF,t_70#pic_center" alt="csdn资源仓库" style="zoom:50%;" />

##### 🆗社区交流群

<img src="https://img-blog.csdnimg.cn/20210102004119705.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMTUxNw==,size_16,color_FFFFFF,t_70#pic_center" alt="交流群" style="zoom:50%;" />



